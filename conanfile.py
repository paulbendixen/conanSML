from conans import ConanFile


class SmlConan(ConanFile):
    name = "sml"
    version = "1.0.1"
    license = "Boost Software license"
    url = "https://gitlab.com/paulbendixen/conanSML"
    description = "C++14 State Machine"
    no_copy_source = True
    exports_sources = "include/*"
    # Header only library so no settings/options
    settings = { "cppstd": ["14", "gnu14", "17", "gnu17", "20", "gnu20" ] }

    def source(self):
        self.run("git clone https://github.com/boost-experimental/sml.git")

    #no def build( self ): since there is no build step

    def package(self):
        self.copy("sml.hpp", src="sml/include/boost", dst="include/boost")

    def package_id( self ):
	    self.info.header_only()
